FROM debian:latest
COPY target/release/notifications ./app
COPY .env .
RUN apt-get update && apt-get -y upgrade 
CMD ["./app"]
