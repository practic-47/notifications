extern crate serde;
extern crate serde_derive;
extern crate serde_json;
extern crate tgbot;

use practic_events_lib::*;
use std::env;

#[tokio::main]
async fn main() {
    use dotenv;
    dotenv::dotenv().ok();
    let _messaging = messaging::build();
    let bot = tokio::spawn(telegram_bot::build());
    bot.await.expect("Failed to await bot.");
}

mod messaging {
    use crate::*;
    use carapax::{methods::SendMessage, Api, Config};
    use natsclient::{self, Client, ClientOptions};
    use std::convert::TryFrom;
    use tokio::runtime::Runtime;

    pub fn build() -> Client {
        let client = Client::from_options(
            ClientOptions::builder()
                .cluster_uris(vec![env::var("NATS_URI")
                    .expect("Set NATS_URI environment variable.")
                    .into()])
                .connect_timeout(std::time::Duration::from_millis(1000))
                .build()
                .expect("Failed to build Client Options."),
        )
        .expect("Failed to build Client.");
        client.connect().expect("Failed to connect Client.");
        let token =
            env::var("TELEGRAM_BOT_TOKEN").expect("Set TELEGRAM_BOT_TOKEN environment variable");
        let config = Config::new(token);
        let api = Api::new(config).expect("Failed to create API");
        client
            .subscribe("notifications", move |msg| {
                println!("Notification Event {:?}.", msg);
                let tg_notification = TelegramNotification::try_from(msg.payload.clone())
                    .expect("Failed to deserialize message.");
                Runtime::new()
                    .expect("Failed to create new Runtime.")
                    .block_on(api.execute(SendMessage::new(
                        tg_notification.chat_id.clone(),
                        tg_notification.message,
                    )))
                    .expect("Failed to execute Send Message command.");
                Ok(())
            })
            .expect("Failed to subscribe.");
        client
    }

    #[test]
    fn test_client_connect_ok() {
        dotenv::dotenv().ok();
        let _client = build();
    }

    #[test]
    fn test_client_subscrib_receive_message() {
        use std::convert::TryInto;
        dotenv::dotenv().ok();
        let client = build();
        let message: Vec<u8> = TelegramNotification {
            chat_id: 4103090,
            message: "Test.".to_string(),
        }
        .try_into()
        .expect("Failed to serialize message.");
        client
            .publish("notifications", &message, None)
            .expect("Failed to publish message.");
        std::thread::sleep(std::time::Duration::from_millis(200));
    }
}

mod telegram_bot {
    use crate::*;
    use carapax::{
        handler, longpoll::LongPoll, methods::SendMessage, Api, Command, Config, Dispatcher,
        ExecuteError,
    };

    pub async fn build() {
        let token =
            env::var("TELEGRAM_BOT_TOKEN").expect("Set TELEGRAM_BOT_TOKEN environment variable");
        let config = Config::new(token);
        let api = Api::new(config).expect("Failed to create API");
        let mut dispatcher = Dispatcher::new(api.clone());
        dispatcher.add_handler(handle_start);
        dispatcher.add_handler(handle_help);
        dispatcher.add_handler(handle_settings);
        LongPoll::new(api, dispatcher).run().await
    }

    #[handler(command = "/start")]
    async fn handle_start(api: &Api, command: Command) -> Result<(), ExecuteError> {
        let message = command.get_message();
        let chat_id = message.get_chat_id();
        let method = SendMessage::new(chat_id, "Hello!");
        let result = api.execute(method).await?;
        println!("SendMessage result: {:?}", result);
        Ok(())
    }

    #[handler(command = "/help")]
    async fn handle_help(api: &Api, command: Command) -> Result<(), ExecuteError> {
        let message = command.get_message();
        let chat_id = message.get_chat_id();
        let method = SendMessage::new(chat_id, "Use `/start`,`/help` and `/settings`.");
        let result = api.execute(method).await?;
        println!("SendMessage result: {:?}", result);
        Ok(())
    }

    #[handler(command = "/settings")]
    async fn handle_settings(api: &Api, command: Command) -> Result<(), ExecuteError> {
        let message = command.get_message();
        let chat_id = message.get_chat_id();
        let method = SendMessage::new(chat_id, "There are no settings right now");
        let result = api.execute(method).await?;
        println!("SendMessage result: {:?}", result);
        Ok(())
    }
}
